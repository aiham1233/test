export interface TreeItem {
  id: number;
  name: string;
  link?:string
  children?: TreeItem[];
}
