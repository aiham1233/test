import { Component, Injectable } from '@angular/core';
import {FlatTreeControl, NestedTreeControl} from '@angular/cdk/tree';
import {MatTreeFlatDataSource, MatTreeFlattener, MatTreeNestedDataSource} from '@angular/material/tree';
import { BehaviorSubject , Observable, of as observableOf} from 'rxjs';
import { TreeItem } from './model/TreeItem.model';
import { AddItemComponent } from './view/add-item/add-item.component';
import { MatDialog } from '@angular/material/dialog';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import { SelectionModel } from '@angular/cdk/collections';
import { MatCheckboxChange } from '@angular/material/checkbox';

export class FileNode {
  id: string;
  children: FileNode[];
  filename: string;
  link : string;
  type: any;
}

export class FileFlatNode {
  constructor(
    public expandable: boolean,
    public filename: string,
    public link:string,
    public level: number,
    public type: any,
    public id: string
  ) {}
}

const TREE_DATA = JSON.stringify({

    parent_1: 'http://',
    parent_2: 'http://',


});


@Injectable()
export class FileDatabase {
  dataChange = new BehaviorSubject<FileNode[]>([]);

  get data(): FileNode[] { return this.dataChange.value; }

  constructor() {
    this.initialize();
  }

  initialize() {
    const dataObject = JSON.parse(TREE_DATA);
    const data = this.buildFileTree(dataObject, 0);
    this.dataChange.next(data);
  }
  buildFileTree(obj: {[key: string]: any}, level: number, parentId: string = '0'): FileNode[] {
    return Object.keys(obj).reduce<FileNode[]>((accumulator, key, idx) => {
      const value = obj[key];
      const node = new FileNode();
      node.filename = key;
      node.id = `${parentId}/${idx}`;

      if (value != null) {
        if (typeof value === 'object') {
          node.children = this.buildFileTree(value, level + 1, node.id);
        } else {
          node.type = value;
        }
      }

      return accumulator.concat(node);
    }, []);
  }
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  providers: [FileDatabase]

})
export class AppComponent {

  treeControl: FlatTreeControl<FileFlatNode>;
  treeFlattener: MatTreeFlattener<FileNode, FileFlatNode>;
  dataSource: MatTreeFlatDataSource<FileNode, FileFlatNode>;
  expansionModel = new SelectionModel<string>(true);
  dragging = false;
  expandTimeout: any;
  expandDelay = 1000;
  validateDrop = false;

  constructor(database: FileDatabase,private dialog:MatDialog) {
    this.treeFlattener = new MatTreeFlattener(this.transformer, this._getLevel,
      this._isExpandable, this._getChildren);
    this.treeControl = new FlatTreeControl<FileFlatNode>(this._getLevel, this._isExpandable);
    this.dataSource = new MatTreeFlatDataSource(this.treeControl, this.treeFlattener);

    database.dataChange.subscribe(data => {
      console.log(data)
      this.rebuildTreeForData(data)});
  }

  transformer = (node: FileNode, level: number) => {
    return new FileFlatNode(!!node.children, node.filename,node.link, level, node.type, node.id);
  }
  private _getLevel = (node: FileFlatNode) => node.level;
  private _isExpandable = (node: FileFlatNode) => node.expandable;
  private _getChildren = (node: FileNode): Observable<FileNode[]> => observableOf(node.children);
  hasChild = (_: number, _nodeData: FileFlatNode) => _nodeData.expandable;


  shouldValidate(event: MatCheckboxChange): void {
    this.validateDrop = event.checked;
  }

  visibleNodes(): FileNode[] {
    const result = [];

    function addExpandedChildren(node: FileNode, expanded: string[]) {
      result.push(node);
      if (expanded.includes(node.id)) {
        node.children.map((child) => addExpandedChildren(child, expanded));
      }
    }
    this.dataSource.data.forEach((node) => {
      addExpandedChildren(node, this.expansionModel.selected);
    });
    return result;
  }
  drop(event: CdkDragDrop<string[]>) {
    if (!event.isPointerOverContainer) return;
    const visibleNodes = this.visibleNodes();

    const changedData = JSON.parse(JSON.stringify(this.dataSource.data));

    function findNodeSiblings(arr: Array<any>, id: string): Array<any> {
      let result, subResult;
      arr.forEach((item, i) => {
        if (item.id === id) {
          result = arr;
        } else if (item.children) {
          subResult = findNodeSiblings(item.children, id);
          if (subResult) result = subResult;
        }
      });
      return result;

    }

    const nodeAtDest = visibleNodes[event.currentIndex];
    const newSiblings = findNodeSiblings(changedData, nodeAtDest.id);
    if (!newSiblings) return;
    const insertIndex = newSiblings.findIndex(s => s.id === nodeAtDest.id);

    const node = event.item.data;
    const siblings = findNodeSiblings(changedData, node.id);
    const siblingIndex = siblings.findIndex(n => n.id === node.id);
    const nodeToInsert: FileNode = siblings.splice(siblingIndex, 1)[0];
    if (nodeAtDest.id === nodeToInsert.id) return;

    const nodeAtDestFlatNode = this.treeControl.dataNodes.find((n) => nodeAtDest.id === n.id);
    if (this.validateDrop && nodeAtDestFlatNode.level !== node.level) {
      alert('Items can only be moved within the same level.');
      return;
    }

    newSiblings.splice(insertIndex, 0, nodeToInsert);

    this.rebuildTreeForData(changedData);
  }

  dragStart() {
    this.dragging = true;
  }
  dragEnd() {
    this.dragging = false;
  }
  dragHover(node: FileFlatNode) {
    if (this.dragging) {
      clearTimeout(this.expandTimeout);
      this.expandTimeout = setTimeout(() => {
        this.treeControl.expand(node);
      }, this.expandDelay);
    }
  }
  dragHoverEnd() {
    if (this.dragging) {
      clearTimeout(this.expandTimeout);
    }
  }


  rebuildTreeForData(data: any) {
    this.dataSource.data = data;
    console.log(this.dataSource)
    this.dataSource.data=[...this.dataSource.data]
    this.expansionModel.selected.forEach((id) => {
        const node = this.treeControl.dataNodes.find((n) => n.id === id);
        this.treeControl.expand(node);
      });
  }

  private expandNodesById(flatNodes: FileFlatNode[], ids: string[]) {
    if (!flatNodes || flatNodes.length === 0) return;
    const idSet = new Set(ids);
    return flatNodes.forEach((node) => {
      if (idSet.has(node.id)) {
        this.treeControl.expand(node);
        let parent = this.getParentNode(node);
        while (parent) {
          this.treeControl.expand(parent);
          parent = this.getParentNode(parent);
        }
      }
    });
  }

  add_parent(){

    let row :TreeItem;
   const dialogRef=   this.dialog.open(AddItemComponent,{
     data :{data:row},
     width:'600px'
   })

   dialogRef.afterClosed().subscribe(res=> {
    let a=new FileNode()
    a.filename=res.result.name;
    a.type=res.result.link;
    a.id="0/"+this.dataSource.data.length.toString()
    this.dataSource.data.push(a)
    console.log(this.dataSource.data)
    this.rebuildTreeForData(this.dataSource.data)
   })
  }

  private getParentNode(node: FileFlatNode): FileFlatNode | null {
    const currentLevel = node.level;
    if (currentLevel < 1) {
      return null;
    }
    const startIndex = this.treeControl.dataNodes.indexOf(node) - 1;
    for (let i = startIndex; i >= 0; i--) {
      const currentNode = this.treeControl.dataNodes[i];
      if (currentNode.level < currentLevel) {
        return currentNode;
      }
    }
    return null;
  }

    add_child(node?){
      console.log(node)
      console.log(this.findById(this.dataSource.data,node.id,'children'))


      let parent =this.findById(this.dataSource.data,node.id,'children')
      if(!parent.hasOwnProperty('children')){
         parent.children=[]
      }
      let row :TreeItem;
      const dialogRef=   this.dialog.open(AddItemComponent,{
        data :{data:row},
        width:'600px'
      })

      dialogRef.afterClosed().subscribe(res=> {
       let a=new FileNode()
       a.filename=res.result.name;
       a.type=res.result.link;
       a.id=parent.id+"/"+parent.children.length.toString()
       parent.children.push(a);
       console.log(this.dataSource.data)
       this.rebuildTreeForData(this.dataSource.data)
      })

}

 findById(arr, id, nestingKey) {
  if(arr.length == 0) return

  return arr.find(d => d.id == id)
      || this.findById(arr.flatMap(d => d[nestingKey] || []), id,nestingKey)
      || 'Not found'
}


edit(node){
  console.log(node)


  let parent =this.findById(this.dataSource.data,node.id,'children')

  let row :TreeItem={
    name:node.filename,
    link:node.type,
    id:node.id
  };

  const dialogRef=   this.dialog.open(AddItemComponent,{
    data :{data:row},
    width:'600px'
  })

  dialogRef.afterClosed().subscribe(res=> {
   parent.filename=res.result.name;
   parent.type=res.result.link
   this.rebuildTreeForData(this.dataSource.data)
  })
}
}
