import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ItemModel } from 'src/app/model/item.model';
import { MenuModel } from 'src/app/model/menu.model';
import { TreeItem } from 'src/app/model/TreeItem.model';

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.css']
})
export class AddItemComponent implements OnInit {

  constructor(
    private dialogRef:MatDialogRef<AddItemComponent>,
    @Inject(MAT_DIALOG_DATA) public data : any,
    private fb:FormBuilder) { }
    item : TreeItem;
    ShowSpinner = false;
    ItemForm: FormGroup;
ngOnInit(): void {
  console.log(this.data.data)


  this.createForm();
  if(this.data.data){
    // if(this.item.id!=0){
    this.fillFormWithData()
  // }
  }

}
createForm(){
  this.ItemForm = this.fb.group({
    name : ['',Validators.required],
    link:['',Validators.required]
  })
}
fillFormWithData(){
  let obj ={
    'name':this.data.data.name,
    'link':this.data.data.link
  }
  this.ItemForm.patchValue(obj);
}
onSubmit(){
  let obj = this.preaperInfo()
  console.log(obj)
  if(this.ItemForm.invalid){
    Object.keys(this.ItemForm.controls).forEach(controlName =>
      this.ItemForm.controls[controlName].markAsTouched()
    );
    return;
  }
  this.createItem(obj)
  // this.item.id==0 ? this.createItem(obj) : this.editItem(obj)
}
createItem(item){
        this.dialogRef.close({isEdit:false,result:item})
}
editItem(item){
  this.ShowSpinner = true;
}
preaperInfo(){
  const controls = this.ItemForm.controls;

  const property:any={
    name : controls['name'].value,
    link : controls['link'].value,

  }

  return property
}


getMode() {
  if (this.data.data) {
    return "edit";
  }
  else return 'add'
}
}
